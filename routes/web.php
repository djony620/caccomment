<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\DB;

$router->get('/{id}', function ($id) use ($router) {

    try {
        $commentDetails =  app('db')->select('select u.photo,c.message,c.type,c.data_id from comments c join users u on c.user_id = u.id  where c.id='.$id)[0];
        if ($commentDetails->type== 1){
            $data['title'] = app('db')->select('select id,title from news where id='.$commentDetails->data_id)[0];
            $data['url'] = 'news-details';

        }
        elseif ($commentDetails->type== 2){
            $data['title'] = app('db')->select('select id,title from how_tos where id='.$commentDetails->data_id)[0];
            $data['url'] = 'how-to-details';
        }
        elseif ($commentDetails->type== 3){

            $data['title'] = app('db')->select('select id,category_title as title from besk_pick_categories where id='.$commentDetails->data_id)[0];
            $data['url'] = 'best-pick-details';
        }

        elseif ($commentDetails->type== 4){
            $data['title'] = app('db')->select('select id,title from reviews where id='.$commentDetails->data_id)[0];
            $data['url'] = 'review-details';
        }

        elseif ($commentDetails->type== 5){
            $data['title'] = app('db')->select('select d.id,dd.device_name as title from devices d join device_details dd on d.id = dd.device_id where d.id='.$commentDetails->data_id)[0];
            $data['url'] = 'device-details';

        }
        $data['url'] = env('WEB_SITE_URL').$data['url']."/".$data['title']->id.'/'.\Illuminate\Support\Str::lower(str_replace(' ','-',$data['title']->title))."#comment-".$id;

            $data['data'] = $commentDetails;

        return view('index',$data);


    }catch (\Exception $exception){
          return  view('404');
    }

    //news=1
    //howto=2
    //bestpickcategory=3
    //review=4
    //device=5

});
