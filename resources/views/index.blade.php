<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <!-- Update your html tag to include the itemscope and itemtype attributes. -->
    <!-- Place this data between the <head> tags of your website -->

    <title>{{$title->title}}</title>
    @php
    $img = env('PHOTO_URL').'/'.$data->photo;
    @endphp
    <meta name="description" content="{{strip_tags($data->message)}}" />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{$title->title}}">
    <meta itemprop="description" content="{{strip_tags($data->message)}}">
    <meta itemprop="image" content="{{$img}}">


    <!-- Open Graph data -->
    <meta property="og:title" content="{{$title->title}}" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{$img}}" />
    <meta property="og:description" content="{{strip_tags($data->message)}}" />
    <meta property="og:site_name" content="cacPhone" />

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="{{$img}}">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{$title->title}}">
    <meta name="twitter:description" content="{{strip_tags($data->message)}}">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{$img}}">



</head>
<body>




    <script type="text/javascript">
        window.location = "{{$url}}";//here double curly bracket
    </script>

</body>
</html>
